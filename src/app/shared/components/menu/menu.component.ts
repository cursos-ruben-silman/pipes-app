import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'shared-menu',
  templateUrl: './menu.component.html',
  styles: [
  ]
})
export class MenuComponent implements OnInit {
  public menuItems: MenuItem[] = [];

  ngOnInit() {
      this.menuItems = [
          {
              label: 'Pipes de angular',
              icon: 'pi pi-desktop',
              items: [
                {
                  label: 'Textos y fechas',
                  icon: 'pi pi-align-left',
                  routerLink: 'numbers'
                },
                {
                  label: 'Números',
                  icon: 'pi pi-dollar',
                  routerLink: 'uncommon'
                },
                {
                  label: 'No comunes',
                  icon: 'pi pi-globe',
                  routerLink: ''
                },
              ]
          },
          {
            label: 'Pipes personalizados',
            icon: 'pi pi-cog',
            items: [
              {
                label: 'Otro elemento',
                icon: 'pi pi-cog',
              }
            ]
          }
      ];
  }

}
